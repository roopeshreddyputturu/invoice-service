package com.treemindtech.invoiceservice.config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.treemindtech.invoiceservice.schedular.InvoiceSchedular;

@Component
public class LoadSchedular implements ServletContextListener {

	@Autowired
	private InvoiceSchedular invoice;

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		invoice.invoiceScheduler();
	}
}

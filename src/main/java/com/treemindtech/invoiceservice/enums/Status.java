package com.treemindtech.invoiceservice.enums;

public enum Status {

	PAID, UNPAID
}

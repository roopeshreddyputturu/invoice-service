package com.treemindtech.invoiceservice.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.treemindtech.invoiceservice.entity.InvoiceEntity;
import com.treemindtech.invoiceservice.enums.Status;

@Repository
public interface InvoiceRepo extends JpaRepository<InvoiceEntity, Integer> {

	List<InvoiceEntity> findByStatus(Status status);

}

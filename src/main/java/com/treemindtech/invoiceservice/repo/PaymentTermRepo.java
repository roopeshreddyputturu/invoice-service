package com.treemindtech.invoiceservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.treemindtech.invoiceservice.entity.PaymentTermEntity;

@Repository
public interface PaymentTermRepo extends JpaRepository<PaymentTermEntity, Integer> {

	
	public PaymentTermEntity findByCode(String code);
	
}

package com.treemindtech.invoiceservice.schedular;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.treemindtech.invoiceservice.entity.InvoiceEntity;
import com.treemindtech.invoiceservice.entity.PaymentTermEntity;
import com.treemindtech.invoiceservice.enums.Status;
import com.treemindtech.invoiceservice.repo.InvoiceRepo;
import com.treemindtech.invoiceservice.repo.PaymentTermRepo;

@Component
@EnableScheduling
public class InvoiceSchedular {

	@Autowired
	private InvoiceRepo invoiceRepo;

	@Autowired
	private PaymentTermRepo paymentRepo;

	private static final Logger logger = LoggerFactory.getLogger(InvoiceSchedular.class);

	@Scheduled(cron = "0 0 0 * * *")
	@Transactional
	public void invoiceScheduler() {

		List<InvoiceEntity> invoiceList = new ArrayList<>();
		try {
			invoiceList = invoiceRepo.findByStatus(Status.UNPAID);
			if (!invoiceList.isEmpty()) {
				invoiceList.forEach(invoice -> {
					PaymentTermEntity pe = paymentRepo.findByCode(invoice.getPaymentTerm());
					LocalDate checkDate = invoice.getInvoiceDate().plusDays(pe.getDays());
					if (checkDate.minusDays(pe.getReminderDays()).equals(LocalDate.now())) {
						logger.error(" Reminder sent for Invoice  : " + invoice.getInvoiceNo());
					}
				});
			} else {
				logger.error(" ** NO PENDING INVOICES  **");
			}

		} catch (Exception e) {
			logger.error("failed to run invoice schedular ::: ", e);
		}
	}

}
